# DemoApp

* favori ekleme hariç diğer modüller geliştirildi.
* vakit çok kısıtlı olduğu için herhangi bir mimari kullanmadım.
* Mümkün mertebe düzgün bir package yapısı oluşturmaya çalıştım.
* Normalde daha temiz kod yazardım ancak vakit kısıtlı olduğu için biraz çirkin gözükebilir.
* Daha fazla zamanım olsaydı uygulamayı kotlinle yazardım ve favori ekleme modülünüde geliştirirdim. Belki birkaç görsel değişiklik daha yapardım.
* Uygulama halihazırda sorunsuz çalışmaktadır. Dilerseniz imzalı apk oluşturarakta bakabilirsiniz.
* Uygulamayı çalıştırmak için herhangi bir toola ihtiyaç yoktur.
