package com.albatross.mobvendemo.Network;

import com.albatross.mobvendemo.Model.Result;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ApiHelper {

    private static ApiHelper instance;
    ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
    String apiKey = "07678bc1d144be114ff633ff3c804eee";

    private ApiHelper() {
    }

    public static synchronized ApiHelper getInstance() {
        if (instance == null) {
            instance = new ApiHelper();
        }
        return instance;
    }


    public void getMovies(int page, int type, ResponseListener responseListener) {

        String key = "";
        if (type == 0)
            key = "top_rated";
        if (type == 1)
            key = "upcoming";
        if (type == 2)
            key = "now_playing";

        Call<Result> callMovies = apiInterface.getMovies("movie/" + key + "?api_key=" + apiKey + "&language=en-US&page=" + String.valueOf(page));
        callMovies.enqueue(new Callback<Result>() {
            @Override
            public void onResponse(Call<Result> call, Response<Result> response) {
                responseListener.onSuccess(response.body());
            }

            @Override
            public void onFailure(Call<Result> call, Throwable t) {
                responseListener.onError(t.getMessage());
            }
        });
    }


    public void searchMovies(int page, String query, ResponseListener responseListener) {

        Call<Result> callMovies = apiInterface.getMovies("search/movie/?api_key=" + apiKey + "&query=" + query + "&page=" + String.valueOf(page));
        callMovies.enqueue(new Callback<Result>() {
            @Override
            public void onResponse(Call<Result> call, Response<Result> response) {
                responseListener.onSuccess(response.body());
            }

            @Override
            public void onFailure(Call<Result> call, Throwable t) {
                responseListener.onError(t.getMessage());
            }
        });

    }


}
