package com.albatross.mobvendemo.Network;

public interface ResponseListener<T> {

    void onSuccess(T response);

    void onError(T error);
}
