package com.albatross.mobvendemo.Network;


import com.albatross.mobvendemo.Model.Movie;
import com.albatross.mobvendemo.Model.Result;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Url;

public interface ApiInterface {

    @GET
    Call<Result> getMovies(@Url String url);
}
