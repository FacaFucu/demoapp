package com.albatross.mobvendemo.Activity;


import android.os.Bundle;
import android.os.StrictMode;
import android.view.MenuItem;

import com.albatross.mobvendemo.R;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;


public class MovieDetailActivity extends AppCompatActivity {

  /*  @BindView(R.id.imageviewMovie)
    AppCompatImageView imageView;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.title)
    TextView toolbarTitle;
    @BindView(R.id.twTur)
    TextView textViewTur;
    @BindView(R.id.twImdb)
    TextView textViewImdb;
    @BindView(R.id.twYonetmen)
    TextView textViewYonetmen;
    @BindView(R.id.twSure)
    TextView textViewSure;
    @BindView(R.id.twUlke)
    TextView textViewUlke;
    @BindView(R.id.twOyuncular)
    TextView textViewOyuncular;
    @BindView(R.id.aciklama)
    TextView textViewAciklama;
    @BindView(R.id.twTarih)
    TextView textViewTarih;


    Context mContext;
    Movie movie = null;*/

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie_detail);
       /* mContext = this;
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        movie = (Movie) getIntent().getExtras().getSerializable("movie");
        toolbarTitle.setText(movie.getFilm().getTitle());*/


        initialize();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }

    }

    private void initialize() {
       /* FilmDetay filmDetay = movie.getFilmDetay();
        Glide.with(mContext)
                .load(Uri.parse("https:" + movie.getFilm().getImageLink()))
                .into(imageView);
        textViewImdb.setText(filmDetay.getImdb());
        textViewTur.setText(filmDetay.getTur().trim());
        textViewYonetmen.setText(filmDetay.getYonetmen().trim());
        textViewSure.setText(filmDetay.getSure().trim());
        textViewUlke.setText(filmDetay.getUlke().trim());
        textViewAciklama.setText(filmDetay.getAciklama().substring(1).trim());
        textViewTarih.setText(filmDetay.getVizyonTarihi().trim());
        String oyuncular = "";
        for (int i = 0; i < movie.getOyuncular().size(); i++) {
            oyuncular = oyuncular + "\n" + movie.getOyuncular().get(i).getAdSoyad();
        }

        oyuncular = oyuncular.substring(1).trim();
        textViewOyuncular.setText(oyuncular);*/


    }

  /*  @OnClick(R.id.btnFragmanIzle)
    public void showFragman(View view) {
       // Helper.showToast(movie.getFilm().getFragman(), mContext);
        //https://www.youtube.com/watch?v=EGINBq8tReQ
        Intent intent = new Intent(MovieDetailActivity.this, YoutubePlayerActivity.class);
        intent.putExtra("id", movie.getFilm().getFragman());
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        startActivity(intent);
    }*/
}
