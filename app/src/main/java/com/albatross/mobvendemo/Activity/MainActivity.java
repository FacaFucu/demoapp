package com.albatross.mobvendemo.Activity;

import android.content.Context;
import android.os.Bundle;
import android.os.StrictMode;
import android.view.View;

import com.albatross.mobvendemo.Adapter.CategoryPagerAdapter;
import com.albatross.mobvendemo.Fragment.MovieFragment;
import com.albatross.mobvendemo.R;
import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;
import java.util.List;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;
import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.tablayout)
    TabLayout tabLayout;
    @BindView(R.id.viewpager)
    ViewPager viewPager;

    List<Fragment> fragmentList;
    CategoryPagerAdapter mPagerAdapter;
    int mFragmentCount = 3;
    Context mContext;
    String[] fragmentNameList = {"Top Rated", "Upcoming", "Now Playing"};


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mContext = this;
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);

        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        addTabs();
        initializeFragments();
    }


    private void initializeFragments() {

        fragmentList = new ArrayList<>();


        for (int i = 0; i < mFragmentCount; i++) {
            fragmentList.add(MovieFragment.newInstance(i));
        }
        mPagerAdapter = new CategoryPagerAdapter(getSupportFragmentManager(), fragmentList, fragmentNameList, mContext);
        viewPager.setOffscreenPageLimit(mFragmentCount);
        viewPager.setAdapter(mPagerAdapter);
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
        tabLayout.setupWithViewPager(viewPager);

    }

    private void addTabs() {
        for (int i = 0; i < mFragmentCount; i++) {
            tabLayout.addTab(tabLayout.newTab().setText(fragmentNameList[i]));

        }
    }

    public void hideTabLayout() {
        tabLayout.setVisibility(View.GONE);
    }

    public void showTabLayout() {
        tabLayout.setVisibility(View.VISIBLE);
    }
}
