package com.albatross.mobvendemo.Interface;


import android.view.View;

import com.albatross.mobvendemo.Model.Movie;

public interface MovieClickListener {

  public void onItemClick(View view, int position, Movie movie);
}
