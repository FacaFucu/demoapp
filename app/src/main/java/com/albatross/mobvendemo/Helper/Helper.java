package com.albatross.mobvendemo.Helper;

import android.app.Activity;
import android.content.Context;
import android.widget.Toast;


public class Helper {


    public static boolean isNullOrWhiteSpace(final String string) {
        return string == null || string.trim().equals("null") || string.trim().equals("") || string.trim().equals("\"\"");
    }

    public static void showToast(String text, Context context) {
        if (context != null && !((Activity) context).isFinishing())
            Toast.makeText(context, text, Toast.LENGTH_LONG).show();
    }
}
