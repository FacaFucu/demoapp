package com.albatross.mobvendemo.Adapter;

import android.content.Context;

import java.util.ArrayList;
import java.util.List;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

public class CategoryPagerAdapter extends FragmentStatePagerAdapter {

    private List<Fragment> mFragmentList = new ArrayList<>();
    private String[] mFragmentTitleList;
    Context mContext;


    public CategoryPagerAdapter(FragmentManager fm, List<Fragment> fragmentList, String[] fragmentTitleList, Context context) {
        super(fm);
        this.mFragmentList = fragmentList;
        this.mFragmentTitleList = fragmentTitleList;
        this.mContext = context;
    }

    @Override
    public Fragment getItem(int position) {
        return mFragmentList.get(position);
    }

    @Override
    public int getCount() {
        return mFragmentList.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return mFragmentTitleList[position];

    }
}
