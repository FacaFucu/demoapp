package com.albatross.mobvendemo.Adapter;

import android.content.Context;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.albatross.mobvendemo.Interface.MovieClickListener;
import com.albatross.mobvendemo.Model.Movie;
import com.albatross.mobvendemo.R;
import com.bumptech.glide.Glide;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class MoviesAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<Movie> movieList = null;
    private Context mContext;
    private MovieClickListener itemClickListener;

    private class ItemViewHolder extends RecyclerView.ViewHolder {

        public ImageView imageView;
        public TextView title;
        public TextView imdb;

        public ItemViewHolder(View view) {
            super(view);
            imageView = (ImageView) view.findViewById(R.id.thumbnail);
            title = (TextView) view.findViewById(R.id.title);
            imdb = (TextView) view.findViewById(R.id.textviewImdb);
        }
    }

    public MoviesAdapter(Context mContext, List<Movie> movieList, MovieClickListener itemClickListener) {
        this.mContext = mContext;
        this.movieList = movieList;
        this.itemClickListener = itemClickListener;
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View itemView = LayoutInflater.from(mContext)
                .inflate(R.layout.layout_movies_row, viewGroup, false);
        return new ItemViewHolder(itemView);

    }


    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder myViewHolder, int i) {
        populateItemRows((ItemViewHolder) myViewHolder, i);
    }

    @Override
    public int getItemCount() {

        return movieList == null ? 0 : movieList.size();
    }

    private void populateItemRows(ItemViewHolder viewHolder, int position) {

        Glide.with(mContext)
                .load(Uri.parse("https://image.tmdb.org/t/p/w500" + movieList.get(position).getPosterPath()))
                .into(viewHolder.imageView);

        viewHolder.title.setText(movieList.get(position).getOriginalTitle());
        viewHolder.imdb.setText("imdb " + String.valueOf(movieList.get(position).getVoteAverage()));
        viewHolder.imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                itemClickListener.onItemClick(view, position, movieList.get(position));
            }
        });


    }

    public void clearList() {
        final int size = movieList.size();
        movieList.clear();
        notifyItemRangeRemoved(0, size);
        notifyDataSetChanged();
    }
}