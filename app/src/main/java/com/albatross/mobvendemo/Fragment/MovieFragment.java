package com.albatross.mobvendemo.Fragment;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.albatross.mobvendemo.Activity.MainActivity;
import com.albatross.mobvendemo.Adapter.MoviesAdapter;
import com.albatross.mobvendemo.Helper.Helper;
import com.albatross.mobvendemo.Helper.SeparatorDecoration;
import com.albatross.mobvendemo.Interface.MovieClickListener;
import com.albatross.mobvendemo.Model.Movie;
import com.albatross.mobvendemo.Model.Result;
import com.albatross.mobvendemo.Network.ApiHelper;
import com.albatross.mobvendemo.Network.ResponseListener;
import com.albatross.mobvendemo.R;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.SearchView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class MovieFragment extends Fragment implements MovieClickListener {

    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.recyclerview)
    RecyclerView recyclerViewMovies;

    Context mContext;
    int index = 1;
    boolean isLoading = false;
    RecyclerView.LayoutManager mLayoutManager = null;
    List<Movie> movieList = null;
    MoviesAdapter moviesAdapter = null;
    int mType = 0;
    boolean isSearch = false;
    String mQuery = "";


    public MovieFragment() {
    }

    public static MovieFragment newInstance(int type) {
        MovieFragment myFragment = new MovieFragment();
        Bundle args = new Bundle();
        args.putInt("type", type);
        myFragment.setArguments(args);
        return myFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = getActivity();
        setHasOptionsMenu(true);
        mType = getArguments().getInt("type", 0);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_movie, container, false);
        mLayoutManager = new GridLayoutManager(mContext, 3);
        ButterKnife.bind(this, v);
        getMovies(index);

        return v;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initScrollListener();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_fragment, menu);
        super.onCreateOptionsMenu(menu, inflater);

    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        MenuItem searchViewMenuItem = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) searchViewMenuItem.getActionView();
        searchView.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                ((MainActivity) getActivity()).showTabLayout();
                isSearch = false;
                mQuery = "";
                moviesAdapter.clearList();
                index = 1;
                getMovies(index);
                return false;
            }
        });



        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                ((MainActivity) getActivity()).hideTabLayout();
                mQuery = query;
                isSearch = true;
                index = 1;
                searchMovies(index, mQuery);
                searchView.clearFocus();
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                ((MainActivity) getActivity()).hideTabLayout();
                return false;
            }
        });
    }

    @Override
    public void onItemClick(View view, int position, Movie movie) {

    }

    private void getMovies(int index) {
        ApiHelper.getInstance().getMovies(index, mType, new ResponseListener() {
            @Override
            public void onSuccess(Object response) {
                progressBar.setVisibility(View.GONE);
                Result result = (Result) response;
                movieList = result.getMovies();
                moviesAdapter = new MoviesAdapter(mContext, movieList, MovieFragment.this::onItemClick);
                recyclerViewMovies.setLayoutManager(mLayoutManager);
                recyclerViewMovies.addItemDecoration(new SeparatorDecoration(5));
                recyclerViewMovies.setHasFixedSize(true);
                recyclerViewMovies.setItemAnimator(new DefaultItemAnimator());
                recyclerViewMovies.setAdapter(moviesAdapter);

            }

            @Override
            public void onError(Object error) {
                Helper.showToast(error.toString(), mContext);
                progressBar.setVisibility(View.GONE);

            }
        });

    }

    private void searchMovies(int index, String query) {
        ApiHelper.getInstance().searchMovies(index, query, new ResponseListener() {
            @Override
            public void onSuccess(Object response) {
                progressBar.setVisibility(View.GONE);
                Result result = (Result) response;
                movieList = result.getMovies();
                moviesAdapter.clearList();
                moviesAdapter = new MoviesAdapter(mContext, movieList, MovieFragment.this::onItemClick);
                recyclerViewMovies.setLayoutManager(mLayoutManager);
                recyclerViewMovies.addItemDecoration(new SeparatorDecoration(5));
                recyclerViewMovies.setHasFixedSize(true);
                recyclerViewMovies.setItemAnimator(new DefaultItemAnimator());
                recyclerViewMovies.setAdapter(moviesAdapter);
            }

            @Override
            public void onError(Object error) {
                Helper.showToast(error.toString(), mContext);
                progressBar.setVisibility(View.GONE);
            }
        });
    }

    private void initScrollListener() {

        recyclerViewMovies.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (!isLoading) {
                    if (mLayoutManager != null && ((GridLayoutManager) mLayoutManager).findLastCompletelyVisibleItemPosition() == movieList.size() - 1) {
                        loadMore();
                        isLoading = true;
                    }
                }
            }
        });
    }

    private void loadMore() {
        index += 1;
        progressBar.setVisibility(View.VISIBLE);

        if (isSearch) {
            ApiHelper.getInstance().searchMovies(index, mQuery, new ResponseListener() {
                @Override
                public void onSuccess(Object response) {
                    Result result = (Result) response;
                    progressBar.setVisibility(View.GONE);
                    List<Movie> newList = ((Result) response).getMovies();
                    movieList.addAll(newList);
                    moviesAdapter.notifyDataSetChanged();
                    isLoading = false;
                }

                @Override
                public void onError(Object error) {
                    Helper.showToast(error.toString(), mContext);
                    progressBar.setVisibility(View.GONE);
                }
            });
        } else {
            ApiHelper.getInstance().getMovies(index, mType, new ResponseListener() {
                @Override
                public void onSuccess(Object response) {
                    Result result = (Result) response;
                    progressBar.setVisibility(View.GONE);
                    List<Movie> newList = ((Result) response).getMovies();
                    movieList.addAll(newList);
                    moviesAdapter.notifyDataSetChanged();
                    isLoading = false;
                }

                @Override
                public void onError(Object error) {
                    progressBar.setVisibility(View.GONE);
                    Helper.showToast(error.toString(), mContext);
                }
            });
        }


    }


}
